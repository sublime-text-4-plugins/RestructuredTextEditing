#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""
try:
    from docutils.core import publish_parts

    # from docutils.core import publish_string
except (ImportError, SystemError):
    raise SystemExit("Required <docutils> Python module not found.")

import sys


if __name__ == "__main__":

    try:
        if sys.stdin.isatty():
            raise RuntimeError("No STDIN passed.")

        rst_data = sys.stdin.read().strip()

        if not rst_data:
            raise RuntimeError("STDIN is empty.")

        settings_overrides = {
            # "input_encoding": "unicode",
            "stylesheet": None
        }
        html_parts = publish_parts(
            source=rst_data,
            source_path=None,
            settings=None,
            writer_name="html5",
            settings_overrides=settings_overrides,
        )

    except Exception as err:
        raise SystemExit(err)

    if html_parts:
        sys.stdout.write(html_parts["body"])

    raise SystemExit(0)
