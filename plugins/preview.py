# -*- coding: utf-8 -*-
import os
import webbrowser

import sublime
import sublime_plugin

from . import events
from . import is_desired_scope
from . import logger
from . import plugin_name
from . import settings
from python_utils import cmd_utils
from python_utils.misc_utils import get_system_tempdir
from python_utils.sublime_text_utils.utils import get_executable_from_settings
from python_utils.sublime_text_utils.utils import get_file_path
from python_utils.sublime_text_utils.utils import get_view_context
from python_utils.sublime_text_utils.utils import substitute_variables
from python_utils.sublime_text_utils.queue import Queue

queue = Queue()

__all__ = ["RstePreviewCommand", "RstePreviewListener"]

_html_template = """<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<title>RestructuredText Editing Fork - Preview</title>
{stylesheets}
</head>
<body>
<div class="content boxed">
{content}
</div>
</body>
</html>
"""
_preview_stylesheets = [
    "${packages}/RestructuredTextEditing/assets/css/bootstrap.min.css",
    "${packages}/RestructuredTextEditing/assets/css/bootstrap.tweaks.css",
]
_stylesheet_link_template = '<link rel="stylesheet" href="{href}" />'
_plugin_id = "RestructuredTextEditingPreview-{}"
_rst_to_html = os.path.realpath(
    os.path.abspath(
        os.path.join(
            os.path.normpath(
                os.path.join(
                    os.path.dirname(__file__), os.pardir, "scripts", "rst_to_html.py"
                )
            )
        )
    )
)


class Storage:
    open_previews = []
    python_path = None
    stylesheets = []

    @classmethod
    def update(cls):
        cls.python_path = get_executable_from_settings(
            None, settings.get("python_path_exec_map", {}).get(sublime.platform(), [])
        )
        cls.stylesheets = substitute_variables(
            get_view_context(None), _preview_stylesheets
        )


@events.on("plugin_loaded")
def on_preview_plugin_loaded():
    """On plugin loaded."""
    queue.debounce(
        Storage.update, delay=1000, key=_plugin_id.format("debounce-update-storage")
    )


@events.on("settings_changed")
def on_preview_settings_changed(settings_obj, **kwargs):
    if any((settings_obj.has_changed("python_path_exec_map"),)):
        queue.debounce(
            Storage.update, delay=1000, key=_plugin_id.format("debounce-update-storage")
        )


class RstePreviewCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        file_path = get_file_path(self.view)
        text = self.view.substr(sublime.Region(0, self.view.size())).encode("utf-8")

        if not text or not file_path:
            sublime.status_message("No content to preview")
            return

        html_file_id = "%d-%d" % (self.view.window().id(), self.view.id())
        html_file_path = os.path.join(
            get_system_tempdir(), plugin_name, html_file_id + ".html"
        )
        cmd = (
            [Storage.python_path, _rst_to_html]
            if Storage.python_path
            else [_rst_to_html]
        )

        with cmd_utils.popen(
            cmd,
            logger=logger,
            bufsize=160 * len(text),
            cwd=os.path.dirname(_rst_to_html),
        ) as proc:
            stdout, stderr = proc.communicate(text)

        if stderr:
            title = "%s Error:" % self.__class__.__name__
            msg = str(stderr.decode("utf-8")).strip()
            msg += "\nNOTE: Most likely, you are trying to preview a Sphinx document with Sphinx directives which Docutils can't handle."
            logger.error(f"{title}\n{msg}")
            sublime.status_message(title + " See console.")
            return

        if stdout:
            os.makedirs(os.path.dirname(html_file_path), exist_ok=True)

            with open(html_file_path, "w", encoding="UTF-8") as temp_file:
                temp_file.write(
                    _html_template.format(
                        stylesheets="\n".join(
                            [
                                _stylesheet_link_template.format(href=s)
                                for s in Storage.stylesheets
                            ]
                        ),
                        content=str(stdout.decode("utf-8")),
                    )
                )

            if html_file_id not in Storage.open_previews:
                Storage.open_previews.append(html_file_id)
                webbrowser.open(html_file_path, new=2, autoraise=True)
            else:
                sublime.status_message("Reload web page")

    def is_enabled(self):
        return is_desired_scope(self.view)

    def is_visible(self):
        return is_desired_scope(self.view)


class RstePreviewListener(sublime_plugin.EventListener):
    def on_close(self, view):
        if view and view.id() and view.window() and view.window().id():
            html_file_id = "%d-%d" % (view.window().id(), view.id())

            if html_file_id in Storage.open_previews:
                del Storage.open_previews[html_file_id]


if __name__ == "__main__":
    pass
