# -*- coding: utf-8 -*-
import sublime
import sublime_plugin

from . import events
from . import logger
from . import settings
from python_utils.sublime_text_utils.completions import create_completions_list
from python_utils.sublime_text_utils.queue import Queue

queue = Queue()

__all__ = ["RsteCompletions"]

_plugin_id = "RestructuredTextEditingCompletions-{}"


class Storage:
    completions_scope = []
    completions = []

    @classmethod
    def update(cls):
        cls.completions_scope = settings.get(
            "completions_scope",
            "text.restructuredtext - source - meta.tag | punctuation.definition.tag.begin | comment.block.documentation.python",
        )
        cls.completions = create_completions_list(
            settings,
            kind=(sublime.KIND_ID_SNIPPET, "R", "RestructuredText Snippet"),
            logger=logger,
        )


@events.on("plugin_loaded")
def on_completions_plugin_loaded():
    """On plugin loaded."""
    queue.debounce(
        Storage.update, delay=1000, key=_plugin_id.format("debounce-update-storage")
    )


@events.on("settings_changed")
def on_completions_settings_changed(settings_obj, **kwargs):
    if any(
        (
            settings_obj.has_changed("completions"),
            settings_obj.has_changed("completions_user"),
            settings_obj.has_changed("completions_scope"),
            settings_obj.has_changed("inhibit_word_completions"),
            settings_obj.has_changed("inhibit_explicit_completions"),
            settings_obj.has_changed("dynamic_completions"),
            settings_obj.has_changed("inhibit_reorder"),
        )
    ):
        queue.debounce(
            Storage.update, delay=1000, key=_plugin_id.format("debounce-update-storage")
        )


class RsteCompletions(sublime_plugin.EventListener):
    def on_query_completions(self, view, prefix, locations):
        if not view or not Storage.completions:
            return None

        if sublime.active_window().active_view().id() != view.id():
            return None

        if not view.match_selector(locations[0], Storage.completions_scope):
            return None

        return Storage.completions


if __name__ == "__main__":
    pass
